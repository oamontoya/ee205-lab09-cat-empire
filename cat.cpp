///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   20_APR_2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
   // Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

   //cout << name << endl;

	return new Cat( name );
}

bool CatEmpire::empty() {
   //traverse the list and determine whether the list is empty
   return topCat == nullptr;
}
	
void CatEmpire::addCat( Cat* newCat ) {  // Add a cat starting at the root
   if(topCat == nullptr) {
      topCat = newCat;
      return;
   }
   addCat(topCat, newCat);
}

void CatEmpire::addCat( Cat* atCat, Cat* newCat ) {  // Add a cat starting at atCat

   if(atCat->name > newCat->name) {
      if(atCat->left == nullptr) {
         atCat->left = newCat;
      } else {
         addCat(atCat->left, newCat);
      }
   } else {
      if(atCat->right == nullptr) {
         atCat->right = newCat;
      } else {
         addCat(atCat->right, newCat);
      }
   }
}

//uses a reverse in-order depth first search to print a family tree of cats
void CatEmpire::catFamilyTree() const {
	//special case: the tree is empty
   if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}
	dfsInorderReverse( topCat, 1 );
}

//visualize a cat family tree from right to left, parents on the left and children on the right
//recursively traverses the tree to the rightmost leaf (the topmost leaf on the sideways family tree)
//prints out that node, then its parent, then traverses to the leftmost leaf (the leaf below the parent node on the sideways family tree)
void CatEmpire::dfsInorderReverse(Cat* atCat, int depth) const {

   if(atCat == nullptr)
      return;
   
   dfsInorderReverse(atCat->right, depth + 1);
   
   //print out spacing as appropriate, then the cats name
   cout << string(6 * (depth - 1), ' ') << atCat->name;
   
   //print out lines pointing to children depending on how many and in which direction they are in relation to the parent
   if((atCat->left == nullptr) && (atCat->right == nullptr)) {
      cout << endl;
   }
   else if((atCat->left != nullptr) && (atCat->right != nullptr)) {
      cout << "<" << endl;
   }
   else{
      if(atCat->left == nullptr)
         cout << "/" << endl;
      else if(atCat->right == nullptr)
         cout << "\\"  << endl;
   }
   
   dfsInorderReverse(atCat->left, depth + 1);
}

//uses an in-order depth-first search to print out an alphabetized list of cats
//the left child of a parent cat has a "smaller" name than the parent
//the right child of a parent cat has a "larger" name than the parent
void CatEmpire::catList() const {
   //special case: the tree is empty
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}
	dfsInorder( topCat );
}

//implement an in-order depth first search
//recursively traverses the binary tree to the leftmost leaf
//prints out that node, then its parent, then traverses to the rightmost leaf
//repeats until the tree has been fully traversed
void CatEmpire::dfsInorder(Cat* atCat) const {
   if(atCat == nullptr)
      return;
   dfsInorder(atCat->left);
   cout << atCat->name << endl;
   dfsInorder(atCat->right);
}

//uses a preorder depth first search to list which children each parent cat produced
void CatEmpire::catBegat() const {
	//special case: the tree is empty
   if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}
   dfsPreorder( topCat );
}

//implement a preorder depth first search
//if the current node is a parent, print their children
//continue to the left child of the parent, repeating until the leftmost leaf has no children
//then traverse to the right children, repeating until the tree is fully traversed
void CatEmpire::dfsPreorder(Cat* atCat) const {
   if(atCat == nullptr)
      return;
   
   //special case: the parent has two children
   if((atCat->left != nullptr) && (atCat->right != nullptr)) {
      cout << atCat->name << " begat " << atCat->left->name << " and " << atCat->right->name << endl;
   }
   //special case: the parent node has no children
   else if((atCat->left == nullptr) && (atCat->right == nullptr)) {
      //print no begats
   }
   //general case: the parent has one child
   else{
      if(atCat->left == nullptr)
         cout << atCat->name << " begat " << atCat->right->name << endl;
      else if(atCat->right == nullptr)
         cout << atCat->name << " begat " << atCat->left->name << endl;
   }
   
   dfsPreorder(atCat->left);
   dfsPreorder(atCat->right);
}

//use a breadth first search to traverse each level of the tree, using a Queue to record how many cats are in the current level
//nullptrs in the list are used as spacing between generations
void CatEmpire::catGenerations() const {
   int generation = 1;
   //the queue holds which cats are actively being counted
   queue<Cat*> catQueue;

   //start with the root cat
   catQueue.push(topCat);
   //follow that cat with a nullptr, signalling the end of the first generation
   catQueue.push(nullptr);
   
   //print the generation, including the ordinal suffix
   cout << generation;
   getEnglishSuffix(generation);
   cout << " generation" << endl;

   //loop while the list has cat nodes in it
   while(!catQueue.empty()) {
      //the current cat is the front of the queue
      Cat* currentCat = catQueue.front();
      //remove that cat from the queue
      catQueue.pop();

      //special case: the current cat is a nullptr, indicating the end of a generation
      if(currentCat == nullptr) {
         //increase the generation
         generation++;
         //add another nullptr to the end of the queue, demarking any future added cats as belonging to the next generation
         catQueue.push(nullptr);
         //special case: the list was empty, and the nullptr we just added is the only thing in the list
         if(catQueue.front() == nullptr) {
            //print a final line break
            cout << endl;
            //end the loop
            break;
         }
         //print the generation, including the ordinal suffix
         cout << endl;
         cout << generation;
         getEnglishSuffix(generation);
         cout << " generation" << endl;
      }
      //general case: the current cat is indeed a cat
      else {
         //print the name of the cat along with some spacing
         cout << currentCat->name << "  ";
         
         //if the cat has children, add them to the queue
         //this will occur behind a nullptr demarking them as belonging to the next generation
         if(currentCat->left != nullptr)
            catQueue.push(currentCat->left);
         if(currentCat->right != nullptr)
            catQueue.push(currentCat->right);
      }
   }
}

//prints an ordinal suffix based on an input number
void CatEmpire::getEnglishSuffix(int n) const {
   //special case: the number is less than or equal to zero
   if(n <= 0)
      //return quietly
      return;
   //special case: the number has a one in the tenths digit
   else if((n % 100) > 9 && (n % 100) < 20){
      cout << "th";
      return;
   }
   //general case
   else {
      //output depends on the ones digit
      switch(n % 10) {
         //special cases
         case 0:
            cout << "th";
            return;
         case 1:
            cout << "st";
            return;
         case 2:
            cout << "nd";
            return;
         case 3:
            cout << "rd";
            return;
         //general case: the ones digit is greater than three
         default:
            cout << "th";
            return;
      }
   }
}
